#!/bin/bash
python3 manage.py makemigrations
python3 manage.py migrate
res="$?"
while [ "$res" != "0" ]
do
    sleep 3;
    python3 manage.py migrate
    celery -A celery_tasks.tasks worker -l INFO > hello.txt &
    res="$?"
done

