from django.urls import path
from django.contrib.auth import views as auth_views

from django.contrib.auth.decorators import login_required
from . import views


app_name = "account"
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='registration/user_login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('profile/',  login_required(views.UserView), name='profile'),
    path('signup/', views.signup, name='signup'),
    path('driver_register/', login_required(views.driver_reg),
         name='driver_register'),
    path('changeinfo/',  login_required(views.changeinfo), name='changeinfo'),
    path('delete_driver/',  login_required(views.deldriver), name='deldriver'),
]
