from django import forms
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import RegexValidator


class UserManager(BaseUserManager):

    def _create_user(self, name, email, password):
        if not name:
            raise ValueError('Users must have a name')
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            name=name,
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, email, password):
        return self._create_user(name, email, password)

    def create_superuser(self, name, email, password):
        return self._create_user(name, email, password)

    def __str__(self):
        return self.email


class UserInfo(AbstractBaseUser):
    id = models.BigAutoField(
        auto_created=True, primary_key=True, serialize=True)
    name = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=128)
    email = models.EmailField(unique=True)  # need validator
    is_driver = models.SmallIntegerField(default=0)

    USERNAME_FIELD = "name"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    class Meta:
        managed = True
        db_table = 'user_info'


class DriverInfo(models.Model):
    TYPE_OF_VEHICLE = [
        ('L', 'Large'),
        ('M', 'Medium'),
        ('S', 'Small')
    ]
    # only exists when user exists
    user_id = models.IntegerField()
    real_name = models.CharField(max_length=64, validators=[
        RegexValidator(
            '^[a-zA-Z ]+$',
            'no number is allowed.',
            'Invalid name'
        )])
    vehicle_type = models.CharField(max_length=7, choices=TYPE_OF_VEHICLE)
    max_capacity = models.IntegerField()  # need validator
    licence_plate = models.CharField(
        max_length=32)  # need validator
    special_info = models.TextField(max_length=128, blank=True, null=True)
    is_del = models.SmallIntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'driver_info'
