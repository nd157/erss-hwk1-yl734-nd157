import re
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import login, authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from django.contrib import messages

from account.models import DriverInfo, UserInfo
from .forms import SignUpForm, DriverCreationForm


def UserView(request):
    current_user = UserInfo.objects.get(pk=request.user.id)
    if current_user.is_driver == 0:
        return render(request, 'registration/profile.html', {'email': current_user.email})
    driver_info = DriverInfo.objects.get(user_id=request.user.id, is_del=0)
    return render(request, 'registration/profile.html', {'is_driver': 1, 'email': current_user.email, 'driver_info': driver_info, 'is_special': driver_info.special_info})


def deldriver(request):
    try:
        current_user = UserInfo.objects.get(pk=request.user.id)
    except ObjectDoesNotExist:
        return HttpResponse("Please login to change your detail!")
    if current_user.is_driver == 0:
        return HttpResponse("You are not a driver yet!")
    current_user.is_driver = 0
    driver_info = DriverInfo.objects.get(user_id=request.user.id, is_del=0)
    driver_info.is_del = 1
    driver_info.licence_plate = " "+driver_info.licence_plate
    current_user.save()
    driver_info.save()
    return redirect('account:profile')


def changeinfo(request):
    try:
        current_user = UserInfo.objects.get(pk=request.user.id)
    except ObjectDoesNotExist:
        return HttpResponse("Please login to change your detail!")
    if request.method == "GET":
        if current_user.is_driver == 0:
            return render(request, 'driver/change_info.html', {'email': current_user.email})
        driver_info = DriverInfo.objects.get(
            user_id=request.user.id, is_del=0)
        return render(request, 'driver/change_info.html', {'is_driver': 1, 'email': current_user.email, 'driver_info': driver_info})
    if(request.POST.get("email") != current_user.email):
        current_user.email = request.POST.get("email")
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if(re.fullmatch(regex, current_user.email)):
            current_user.save()
        else:
            messages.error(request, 'Please enter a valid email!')
            return redirect('account:changeinfo')
    if current_user.is_driver == 0:
        return redirect('account:profile')
    driver_info = DriverInfo.objects.get(user_id=request.user.id, is_del=0)
    if(request.POST.get("real_name") != driver_info.real_name):
        driver_info.real_name = request.POST.get("real_name")
        if(re.match("^[a-zA-Z ]*$", driver_info.real_name)):
            pass
        else:
            messages.error(request, 'Please enter a valid name!')
            return redirect('account:changeinfo')
    if(request.POST.get("vehicle_type") != ''):
        driver_info.vehicle_type = request.POST.get("vehicle_type")
    if(request.POST.get("max_capacity") != ''):
        driver_info.max_capacity = request.POST.get("max_capacity")
        p = {'S': '3', 'M': '6', 'L': '8'}
        if driver_info.max_capacity < '1':
            return HttpResponse("Maximum Capacity must be at least 1")
        if driver_info.vehicle_type not in p:
            return HttpResponse("Please select a correct type!")
        if p[driver_info.vehicle_type] < driver_info.max_capacity:
            messages.error(
                request, 'Correct maximum capacity for each type is S:3 M:6 L:8')
            return redirect('account:changeinfo')
    if(request.POST.get("licence_plate") != driver_info.licence_plate):
        driver_info.licence_plate = request.POST.get("licence_plate")
        if(DriverInfo.objects.filter(is_del=0, licence_plate=driver_info.licence_plate).count() != 0):
            messages.error(request, 'this license plate is used by other')
            return redirect('account:changeinfo')
    if(request.POST.get("special_info") != driver_info.special_info):
        driver_info.special_info = request.POST.get("special_info")
    driver_info.save()
    return redirect('account:profile')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(request, name=user.name, email=user.email,
                                password=raw_password)
            if user is not None:
                login(request, user)
            else:
                print("user is not authenticated")
            return redirect('account:profile')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


def driver_reg(request):
    if request.user.is_driver == 0:
        form = DriverCreationForm(request.POST or None)
        if request.method == 'POST':
            if form.is_valid():
                obj = form.save(commit=False)
                obj.user_id = request.user.id
                current_user = UserInfo.objects.get(pk=request.user.id)
                current_user.is_driver = 1
                current_user.save()
                obj.save()
                return redirect('account:profile')
    else:
        return redirect('account:profile')
    return render(request, 'driver/driver_register.html', {'form': form})
