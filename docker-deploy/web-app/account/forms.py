from lib2to3.pgen2.driver import Driver
from xml.dom import ValidationErr
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import UserInfo, DriverInfo


class SignUpForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = UserInfo
        fields = ("name", "email", "password1", "password2")


class DriverCreationForm(forms.ModelForm):
    real_name = forms.CharField(min_length=5, max_length=50)
    licence_plate = forms.CharField(min_length=5, max_length=10)

    class Meta:
        model = DriverInfo
        fields = ['real_name', 'vehicle_type',
                  'max_capacity', 'licence_plate', 'special_info', ]

    def clean_licence_plate(self, *args, **kwargs):
        licence_plate = self.cleaned_data.get("licence_plate")
        if ' ' in licence_plate:
            raise forms.ValidationError("This is not a valid license number!")
        else:
            return licence_plate

    def clean(self):
        cleaned_data = super().clean()
        print(cleaned_data)
        max_capacity = cleaned_data.get("max_capacity")
        vehicle_type = cleaned_data.get("vehicle_type")
        p = {'S': 3, 'M': 6, 'L': 8}
        if max_capacity < 1:
            raise forms.ValidationError("Please enter a number greater than 0")
        if vehicle_type not in p:
            raise forms.ValidationError("This is not a valid vehicle type!")
        if p[vehicle_type] < max_capacity:
            raise forms.ValidationError(('%(vehicle_type)s type vehicle can hold up to %(max_p)s passengers'),
                                        params={'vehicle_type': vehicle_type, 'max_p': p[vehicle_type]})
            # vehicle_type, "can only hold up to", p[vehicle_type], "passengers")
