
my_sender = 'vancoLynn@yeah.net'  # 发件人邮箱账号
my_pass = 'FKFAFHJMNHNTQDGJ'  # 发件人邮箱密码

my_receiver = 'vancoLynn@yeah.net'  # 收件人邮箱账号，我这边发送给自己


from .tools import send_email


import json

html = '''<u><strong>Your ride has confirmed by a driver.</strong></u> <br />\
        <h3> Your driver information is below:</h3>\
        {content}<br /> <br />\
        Please check your ride information in detail. <br />\
           You can view your confirmation in <a href="{host}">MY RIDE</a> . <br /><br /> \
           <u><strong>Please note: This is a system generated email from an unmonitored mailbox. Please do not respond to this message.</strong></u> <br /> \
            If you have any further question regard to the report, pls call 712525 and assign the issue ticket to FlashGet Support group. <br /> <br /><br />'''

def send_flashget_Email(data, receiver_list):
    print(data)
    print(receiver_list)
    title = "Flashget: Confirm Notification"
    subject = title
    # data = json.loads(data_str)
    content = "<ul>{info}<ul/>"
    cur = []
    label = {"name":"name",
             "vehicle_type":"vehicle_type",
             "detail":"special_info",
             "licence":"licence",
             }
    for key, val in label.items():
        cur.append("<li>{c}</li>".format(c=val+" : "+ data.get(key, "")))
    content = content.format(info="".join(cur))
    cur_html = html.format(content=content, host=data.get("url", ""))
    mail_body = cur_html
    # receiver = receiver_list  # 接收人邮件地址 list
    for r in receiver_list:
        print(r)
        send_email(my_sender, my_pass,subject, mail_body, r)


if __name__ == '__main__':
    data = {"name":"test",
             "vehicle_type":"Large",
             "detail":"special_info",
             "licence":"sq123456",
             "url":"http://127.0.0.1:8001/ride/driver/search/detail?id=29"
             }
    # data_str = json.dumps(data)
    # print(data_str)
    send_flashget_Email(data, [my_receiver])
