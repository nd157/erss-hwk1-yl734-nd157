
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.mime.text import MIMEText
import smtplib
def send_email(sender, password, subject, mail_body, receiver, file_names=list(), file_path=list()):
    """
    :param subject: 邮件标题
    :param mail_body: 邮件正文，可以是文字，也可以是html格式
    :param receiver: 邮件正文
    :param file_names: 邮件接收人
    :return:
    """
    smtpserver = 'smtp.yeah.net'  # smtp设置
    username = sender  # 用户登陆账号
    password = password  # 用户登陆密码

    msg = MIMEMultipart()
    # 邮件正文
    msg.attach(MIMEText(mail_body, 'html', 'utf-8'))
    msg['Subject'] = Header(subject, 'utf-8')
    msg['From'] = username
    msg['To'] = receiver

    # 附件:附件名称用英文
    for i,file_name in enumerate(file_names):
        att = MIMEText(open(file_path[i], 'rb').read(), 'base64', 'utf-8')
        file_name = file_name.split("/")[-1]
        att["Content-Type"] = 'application/octet-stream'
        att['Content-Disposition'] = 'attachment;filename="%s"' % (file_name)
        msg.attach(att)

        # 登录并发送邮件
    try:
        # here
        # smtp =smtplib.SMTP()
        smtp = smtplib.SMTP_SSL(smtpserver,465)
        # smtp.connect(smtpserver)
        smtp.login(username, password)
        smtp.sendmail(sender, receiver, msg.as_string())
    except Exception as e:
        print(e)
        print("邮件发送失败！")
        return False
    else:
        print("邮件发送成功！")
    return True
