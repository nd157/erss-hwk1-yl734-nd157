import threading
from celery_tasks.EmailTools.sentEmail import send_flashget_Email

# _thread_limit = 10000
# _cur = 0
def send_email_with_thread(data, receiver_list):

    try:
        t = threading.Thread(target=send_flashget_Email, args=(data, receiver_list,))
        t.start()
    except:
        # if error, try with sync
        print("Error, try to sync:")
        send_flashget_Email(data, receiver_list)