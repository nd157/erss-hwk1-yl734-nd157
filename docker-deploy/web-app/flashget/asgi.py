"""
ASGI config for flashget project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os
from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter,URLRouter
from . import routing



os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'flashget.settings')

# application = get_asgi_application() # 默认处理http协议
application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": URLRouter(routing.websocket_url_patterns)
    }
)