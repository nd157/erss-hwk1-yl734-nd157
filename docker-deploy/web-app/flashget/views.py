from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


def index(request):
    return render(request, 'index.html')


@login_required(login_url='/account/login')
def DriverView(request):
    if request.user.is_driver == 0:
        return redirect(to='/account/driver_register')
    return render(request, 'driver.html')


class OrdersView(LoginRequiredMixin, TemplateView):
    template_name = 'orders.html'


class TripView(LoginRequiredMixin, TemplateView):
    template_name = 'trip.html'
