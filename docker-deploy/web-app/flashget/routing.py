from django.urls import re_path
from ride_service import consumers


websocket_url_patterns = [
    re_path(r'ride/notify/(?P<ride_id>\w+)/$', consumers.NotificationConsumer.as_asgi()),

]