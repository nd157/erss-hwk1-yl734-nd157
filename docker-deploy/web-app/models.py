# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class DriverInfo(models.Model):
    user_id = models.IntegerField()
    real_name = models.CharField(max_length=64)
    vehicle_type = models.CharField(max_length=32)
    max_capacity = models.IntegerField()
    licence_plate = models.CharField(max_length=32)
    special_info = models.CharField(max_length=128, blank=True, null=True)
    is_del = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'driver_info'


class Hallo(models.Model):
    id = models.AutoField()
    rider = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hallo'


class OrderInfo(models.Model):
    user_id = models.IntegerField()
    is_owner = models.SmallIntegerField()
    ride_id = models.IntegerField()
    status = models.SmallIntegerField()
    people_num = models.IntegerField()
    destination = models.TextField()
    early_arrive_time = models.DateTimeField(blank=True, null=True)
    late_arrive_time = models.DateTimeField(blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    is_share = models.SmallIntegerField()
    other = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order_info'


class Ride(models.Model):
    owner_id = models.IntegerField()
    destination = models.TextField()
    early_arrive_time = models.DateTimeField(blank=True, null=True)
    late_arrive_time = models.DateTimeField(blank=True, null=True)
    status = models.SmallIntegerField()
    total_people_num = models.IntegerField(blank=True, null=True)
    max_people_num = models.IntegerField(blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    driver_id = models.IntegerField(blank=True, null=True)
    driver_name = models.CharField(max_length=20, blank=True, null=True)
    complete_time = models.DateTimeField(blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    vehicle_type = models.CharField(max_length=10)
    is_share = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'ride'


class TestTb(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'test_tb'


class UserInfo(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=50)
    is_driver = models.SmallIntegerField()
    password = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'user_info'
