from django.http import HttpResponse, JsonResponse
from celery_tasks.other_async import send_email_with_thread
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import OrderInfo, Ride, DriverInfo

from . import model_util
import time

config_map = {
    "dest": "destination",
    "etime": "early arrive time",
    "ltime": "late arrive time",
    "num": "people number"
}

vehicle_allow_maximum = {
    "S": 3,
    "M": 6,
    "L": 8
}

vehicle_type_name = {
    "S": "Small",
    "M": "Middle",
    "L": "Large"
}



ERROR = "fail"
SUCCESS = "success"


def build_error_data(reason):
    return {'status': ERROR, 'msg': reason}

# Create your views here.


def is_post(request):
    return request.method == "POST"


def is_get(request):
    return request.method == "GET"


@login_required
def ride_test(request):
    return render(request, "ride_service.html")


def time_window_check(etime, ltime):
    data = None
    format = "%Y-%m-%d %H:%M"
    etimestamp = time.mktime(time.strptime(etime, format))
    ltimestamp = time.mktime(time.strptime(ltime, format))
    if etimestamp > ltimestamp:
        data = build_error_data(
            "early arrive time cannot later than late arrive time!")
    if ltimestamp <= time.time():
        data = build_error_data(
            "late arrive time should greater than current time!")
    return data


def check_vehicle_type(num, vehicle_str):
    vts = vehicle_str.split(',')
    max_allow = 0
    for v in vts:
        max_allow = max(max_allow, vehicle_allow_maximum[v])
    return max_allow > num, max_allow


@login_required
def ride_open(request):
    if is_post(request):
        post_dict = request.POST
        is_valid, rep = info_check(post_dict)
        if not is_valid:
            return JsonResponse(rep)
        num_str = post_dict["num"]
        is_share = 1 if post_dict.get("is_share", True) else 0
        num = int(num_str)
        dest = post_dict.get("dest")
        dest = dest.strip()
        detail = post_dict.get("detail", "")
        user_id = request.user.id
        etime = post_dict["etime"]
        ltime = post_dict["ltime"]
        # check vehicle type
        vehicle_type = post_dict.get("vehicle_type", "").strip()
        if len(vehicle_type) == 0:
            return JsonResponse(build_error_data("You should select the vehicle type you want"))
        is_valid, max_allow_num = check_vehicle_type(num, vehicle_type)
        if not is_valid:
            return JsonResponse(build_error_data(
                "Your passenger number: {pn} but max number for your vehicle type is {vn}".format(pn=num,
                                                                                                  vn=max_allow_num)))
        data = model_util.create_ride(
            user_id, dest, etime, ltime, num, max_allow_num, vehicle_type, detail, is_share)
        if not data or data.get("ride", 0) == 0:
            if not data:
                data = build_error_data("unknown reason in db operation")
            else:
                data = build_error_data(
                    data.get("err", "unknown reason in db operation"))
        else:
            data["status"] = SUCCESS
            data["url"] = "/ride/waiting?ride=" + \
                str(data.get("ride")) + "&order="+str(data.get("order"))

        return JsonResponse(data)
    return JsonResponse(build_error_data("unknown GET"))


@login_required
def share_info_check(request):
    if is_post(request):
        post_dict = request.POST
        is_valid, resp = info_check(post_dict)
        if not is_valid:
            return JsonResponse(resp)
        resp = {
            "url": "&".join(["/ride/sharer/search?dest="+post_dict["dest"], "etime="+post_dict["etime"], "ltime="+post_dict["ltime"], "num="+post_dict["num"]])
        }
        return JsonResponse(resp)
    return HttpResponse(request, "404")


@login_required
def order_edit(request):
    if is_post(request):
        post_dict = request.POST
        # todo 假设不变， 直接 return back
        is_valid, rep = info_check(post_dict)
        if not is_valid:
            return JsonResponse(rep)
        if "id" not in post_dict:
            return JsonResponse(build_error_data("bad request without id"))
        num_str = post_dict["num"]
        num = int(num_str)
        dest = post_dict.get("dest")
        dest = dest.strip()
        etime = post_dict["etime"]
        ltime = post_dict["ltime"]
        id = int(post_dict["id"])
        # share ride cannot change destination
        order = model_util.get_order_by_id(id)
        if not order:
            return JsonResponse(build_error_data("cannot find record: id="+str(id)))
        if order.user_id != request.user.id:
            return JsonResponse(build_error_data("only order user can edit order！"))
        if order.is_share:
            if order.destination != dest:
                return JsonResponse(build_error_data("share order can't change destination, please cancel order"))
        # if order.is_owner
        if order.is_owner:
            if str(order.early_arrive_time) != etime or str(order.late_arrive_time) != ltime:
                return JsonResponse(build_error_data("Share owner can't change time window, please cancel your order"))
        if order.user_id != request.user.id:
            return JsonResponse(build_auth_err())
        # check the num is acceptable
        ride = model_util.get_ride_by_id(order.ride_id)
        if not ride:
            return JsonResponse(build_error_data("cannot find ride record of this order"))
        if ride["status"] == model_util.cancel_by_user:
            return JsonResponse(build_error_data("this ride is cancel by owner, please cancel order"))
        if ride["status"] != model_util.unconfirmed:
            return JsonResponse(build_error_data("you can only edit open ride order"))
        if num > ride["rest_num"] + order.people_num:
            return JsonResponse(build_error_data("your new passenger number is greater than maximum passenger"))
        data = model_util.update_order(id, ride["id"], dest, num, etime, ltime)
        if data:
            return JsonResponse(build_error_data(data["err"]))
        url = "/ride/waiting?ride={ride}&order={order}".format(
            ride=ride["id"], order=order.id)
        return JsonResponse({"url": url})


@login_required
def ride_edit(request):
    if is_post(request):
        post_dict = request.POST
        id = post_dict.get("id", 0)
        ride = model_util.get_ride_by_id(id)
        if not ride:
            return JsonResponse(build_error_data("cannot find ride record"))
        if ride["owner_id"] != request.user.id:
            return JsonResponse(build_auth_err())

        if ride["status"] != model_util.unconfirmed:
            return JsonResponse(build_error_data("only open ride can edit!"))
        vt = post_dict.get("vehicle_type", "")
        is_valid, max_allow = check_vehicle_type(ride["cur_num"], vt)
        if not is_valid:
            return JsonResponse(build_error_data("Your edit vehicle type can't hold all passengers!"))
        is_share = post_dict.get("is_share", 1)
        is_share = 1 if is_share else 0
        detail = post_dict.get("detail", "")
        if is_share != ride["is_share"]:
            order = model_util.update_order_by_ride(id, is_share)
        else:
            order = model_util.get_owner_order_by_ride(id)
        if not order:
            return JsonResponse(build_error_data("can't find order record"))

        data = model_util.update_ride(id, vt, max_allow, is_share, detail)
        if not data:
            return JsonResponse(build_error_data("db update fail, please resubmit"))
        return JsonResponse({"url": "/ride/waiting?ride={ride}&order={order}".format(ride=id, order=order.id)})


def info_check(post_dict):
    # check necessary key
    require = ["dest", "etime", "ltime", "num"]
    for key in require:
        if key not in post_dict or len(post_dict[key].strip()) == 0:
            data = build_error_data(
                "you need to enter {name}".format(name=config_map[key]))
            return False, data
    # time check
    etime = post_dict["etime"]
    ltime = post_dict["ltime"]
    data = time_window_check(etime, ltime)
    if data:
        return False, data
    # check number
    num_str = post_dict["num"]
    if not num_str.isdigit() or int(num_str) <= 0 or num_str[0] == '0' or int(num_str) > 8:
        data = build_error_data(
            "You should enter a valid passenger number(0 < num <= 8)!")
        return False, data
    return True, None

# detail with ride, order, clients


@login_required
def ride_wait(request):
    if is_get(request):
        ride_id = request.GET.get("ride", 0)
        order_id = request.GET.get("order", 0)
        ride = model_util.get_ride_by_id(ride_id)
        if not ride or "err" in ride:
            return JsonResponse(build_error_data("can't find ride record"))
        ride["status"] = model_util.ride_status[ride["status"]]
        vt = ride["vehicle_type"]
        ride["vehicle_type"] = ", ".join(
            [vehicle_type_name[v] for v in vt.split(',')])
        order = model_util.get_order_by_id(order_id)
        if not order:
            return JsonResponse(build_error_data("cannot find order id"+str(order_id)))
        # 如果不是order的user, 没有权利看订单的详情
        if order.user_id != request.user.id:
            return redirect("/ride/service/")
        order_info = {
            "id": order.id,
            "is_owner": order.is_owner == 1,
            "dest": order.destination,
            "num": order.people_num,
            "etime": order.early_arrive_time,
            "ltime": order.late_arrive_time
        }
        order_data = model_util.get_all_order_by_ride(ride["id"])
    return render(request, "ride_waiting.html", {'order_info': order_info, 'info': ride, 'order_data': order_data})


@login_required
def sharer_ride_search(request):
    if is_get(request):
        get_dict = request.GET
        dest = get_dict.get("dest")
        dest = dest.strip()
        num = get_dict.get("num")
        etime = get_dict.get("etime")
        ltime = get_dict.get("ltime")
        info = {
            "dest": get_dict.get("dest"),
            "num": get_dict.get("num"),
            "etime": get_dict.get("etime"),
            "ltime": get_dict.get("ltime")
        }

        ride_data = model_util.share_search(
            dest=dest, etime=etime, ltime=ltime, num=num, user_id=request.user.id)
        return render(request, "sharer_search.html", {"info": info, "search_data": ride_data})

@login_required
def driver_ride_search(request):
    if is_get(request):
        user_id = request.user.id
        driver = model_util.get_driver_by_userid(user_id)
        if not driver:
            return redirect('account:driver_register')
        data = model_util.driver_search(
            driver.vehicle_type, driver.max_capacity, user_id)

    return render(request, "driver_search.html", {'search_data': data})


@login_required
def driver_ride_confirm(request):
    if is_post(request):
        user_id = request.user.id
        post_dict = request.POST
        ride_id = post_dict.get("id", "0")
        ride_obj = model_util.get_ride_by_id(ride_id)
        if ride_obj["status"] == model_util.confirm or ride_obj["status"] == model_util.completed:
            return JsonResponse(build_error_data("The ride is confirmed by other driver"))
        if ride_obj["status"] == model_util.cancel_by_user or ride_obj["status"] == model_util.time_out:
            return JsonResponse(build_error_data("The ride is canceled"))
        data = model_util.confirm_ride(user_id, ride_obj["id"])
        if data:
            return JsonResponse(build_error_data(data["err"]))
        # send email
        driver_info = model_util.get_driver_by_userid(user_id)
        driver_data = {
            "name": driver_info.real_name,
            "vehicle_type": vehicle_type_name[driver_info.vehicle_type],
            "detail": driver_info.special_info,
            "licence": driver_info.licence_plate,
        }
        orders = model_util.get_all_order_by_ride(ride_id)
        emails = set()
        for order in orders:
            uid = order["user_id"]
            user = model_util.get_user_by_id(uid)
            emails.add(user.email)
        # async_send_email.delay(driver_data, list(emails))
        # send_flashget_Email(driver_data, list(emails))
        send_email_with_thread(driver_data, list(emails))
        return JsonResponse({"url": "/ride/driver/ride_detail?id="+ride_id})


# ride_detail without order, no alteration
@login_required
def ride_detail(request):
    if is_get(request):
        id = request.GET.get("id", 0)
        if not id:
            return HttpResponse(request, "404 bad request")
        order_data = model_util.get_all_order_by_ride(id)
        ride_data = model_util.get_ride_by_id(id)
        if "id" in ride_data:
            s = ride_data["status"]
            ride_data["status"] = model_util.ride_status[s]
            vt = ride_data["vehicle_type"]
            ride_data["vehicle_type"] = ", ".join(
                [vehicle_type_name[v] for v in vt.split(',')])
    return render(request, "ride_detail.html", {'order_data': order_data, 'info': ride_data, 'is_driver_search': False})


@login_required
def sharer_join_ride(request):
    clean_session_msg(request)
    if is_post(request):
        post_dict = request.POST
        is_valid, resp = info_check(post_dict)
        if not is_valid:
            return JsonResponse(resp)
        ride_id = int(post_dict.get("ride_id", "0"))
        user_id = request.user.id
        people_num = int(post_dict.get("num"))
        dest = post_dict.get("dest")
        dest = dest.strip()
        etime = post_dict.get("etime")
        ltime = post_dict.get("ltime")
        order = model_util.join_ride(
            user_id, ride_id, people_num, dest, etime, ltime)
        if "err" in order:
            return JsonResponse(build_error_data(order["err"]))
        url = "&".join(
            ["/ride/waiting?ride="+str(ride_id), "order="+str(order["id"])])
        return JsonResponse({"url": url})


def clean_session_msg(request):
    if "err_msg" not in request.session:
        return
    del request.session["err_msg"]


def build_session_msg(req, msg):
    req.session["err_msg"] = msg


@login_required
def order_cancel(request):
    if is_post(request):
        prev = "/ride/waiting?ride={ride}&order={order}"
        order_id = request.POST.get("id", 0)
        order = model_util.get_order_by_id(order_id)
        if not order:
            return JsonResponse(build_error_data("can't find order record"))
        prev = prev.format(ride=order.ride_id, order=order_id)
        if order.user_id != request.user.id:
            return JsonResponse(build_error_data("You don't have permission to cancel this order"))
        res = model_util.cancel_order(order_id)
        if res:
            return JsonResponse(build_error_data(res["err"]))
        is_owner = True if order.is_owner else False
        return JsonResponse({"url": prev, "is_owner": is_owner})


@login_required
def driver_search_detail(request):
    if is_get(request):
        id = request.GET.get("id", 0)
        if not id:
            return redirect("/driver/search/")
        order_data = model_util.get_all_order_by_ride(id)
        ride_data = model_util.get_ride_by_id(id)
        if "id" in ride_data:
            s = ride_data["status"]
            ride_data["status"] = model_util.ride_status[s]
            vt = ride_data["vehicle_type"]
            ride_data["vehicle_type"] = ", ".join(
                [vehicle_type_name[v] for v in vt.split(',')])
    return render(request, "ride_detail.html", {'order_data': order_data, 'info': ride_data, 'is_driver_search': True})


@login_required
def driver_ride_complete(request):
    if is_get(request):
        id = request.GET.get("id", 0)
        if not id:
            return HttpResponse(request, "404 bad request")
        curr_ride = Ride.objects.get(id=id)
        if request.user.id != curr_ride.driver_id:
            return HttpResponse(request, "404 bad request")
        data = model_util.change_ride_status(id, model_util.completed)
        if data != None:
            return HttpResponse(data)
        return redirect('driver_history')


@login_required
def user_order_history(request):
    curr_user = request.user.id
    open = OrderInfo.objects.filter(user_id=curr_user, status=0)
    confirmed = OrderInfo.objects.filter(user_id=curr_user, status=1)
    completed = OrderInfo.objects.filter(user_id=curr_user, status=2)
    return render(request, "order_history.html", {'open': list(open), 'confirmed': list(confirmed), 'completed': list(completed)})


@login_required
def driver_ride_history(request):
    if(request.user.is_driver != 1):
        return redirect("account:driver_register")
    curr_user = request.user.id
    confirmed = Ride.objects.filter(driver_id=curr_user, status=1)
    completed = Ride.objects.filter(driver_id=curr_user, status=2)
    return render(request, "driver_history.html", {'confirmed': list(confirmed), 'completed': list(completed)})


@login_required
def static_ride_detail(request):
    if is_get(request):
        id = request.GET.get("id", 0)
        if not id:
            return HttpResponse("404")
        order_data = model_util.get_all_order_by_ride(id)
        ride_data = model_util.get_ride_by_id(id)
        '''if "id" in ride_data:
            s = ride_data["status"]
            ride_data["status"] = model_util.ride_status[s]
            vt = ride_data["vehicle_type"]
            ride_data["vehicle_type"] = ", ".join(
                [vehicle_type_name[v] for v in vt.split(',')])'''
    return render(request, "static_ride_detail.html", {'order_data': order_data, 'info': ride_data})


def view_driver_info(request):
    if is_get(request):
        id = request.GET.get("id", 0)
        if not id:
            return HttpResponse(request, "404 bad request")
        try:
            driver = DriverInfo.objects.get(user_id=id, is_del=0)
        except:
            return HttpResponse(request, "This user is no longer a driver")
        return render(request, "view_driver_info.html", {"driver": driver})


def build_auth_err():
    return build_error_data("No Authentication to do this operation! You aren't the owner of this record")


@login_required
def driver_filter(request):
    if is_get(request):
        driver = model_util.get_driver_by_userid(request.user.id)
        if not driver:
            redirect("/account/driver_register/")
        data = model_util.search_ride_by_special_info(
            driver.special_info, driver.max_capacity, driver.vehicle_type)
        return render(request, "driver_search.html", {'search_data': data})
