from channels.generic.websocket import WebsocketConsumer
from channels.exceptions import StopConsumer
from asgiref.sync import async_to_sync

SUCCESS = "success"
OP_JOIN = "join"
OP_LISTEN = "listen"
OP_CANCEL = "cancel"
OP_CONFIRM = "confirm"
class NotificationConsumer(WebsocketConsumer):
    def websocket_connect(self, message):
        self.accept()
    def websocket_receive(self, message):
        ride_id = self.scope["url_route"]["kwargs"].get("ride_id", "0")
        if message["text"] == OP_JOIN or message["text"] == OP_CONFIRM or message["text"] == OP_CANCEL:
            async_to_sync(self.channel_layer.group_send)(ride_id, {"type":"send.msg", 'message': message})
            self.send(SUCCESS)
        elif message["text"] == OP_LISTEN:
            async_to_sync(self.channel_layer.group_add)(ride_id, self.channel_name)

    def websocket_disconnect(self, message):
        ride_id = self.scope["url_route"]["kwargs"].get("ride_id", "0")
        async_to_sync(self.channel_layer.group_discard)(ride_id, self.channel_name)
        raise StopConsumer()
    def send_msg(self, event):
        self.send(event["message"]["text"])
# class RideConsumer(WebsocketConsumer):
#     def websocket_connect(self, message):
#         pass
#     def websocket_receive(self, message):
#         pass
#     def websocket_disconnect(self, message):
#         pass