from django.db import models

# Create your models here.


class Ride(models.Model):
    owner_id = models.IntegerField()
    destination = models.TextField()
    early_arrive_time = models.DateTimeField()
    late_arrive_time = models.DateTimeField()
    status = models.SmallIntegerField()
    total_people_num = models.IntegerField()
    max_people_num = models.IntegerField()
    detail = models.TextField(blank=True, null=True)
    driver_id = models.IntegerField(blank=True, null=True)
    driver_name = models.CharField(max_length=20, blank=True, null=True)
    complete_time = models.DateTimeField(blank=True, null=True)
    create_time = models.DateTimeField()
    vehicle_type = models.CharField(max_length=10)
    is_share = models.SmallIntegerField()
    owner_name = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'ride'


class OrderInfo(models.Model):
    user_id = models.IntegerField()
    is_owner = models.SmallIntegerField()
    ride_id = models.IntegerField()
    status = models.SmallIntegerField()
    people_num = models.IntegerField()
    destination = models.TextField()
    early_arrive_time = models.DateTimeField()
    late_arrive_time = models.DateTimeField()
    create_time = models.DateTimeField()
    is_share = models.SmallIntegerField()
    user_name = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'order_info'


class UserInfo(models.Model):  # don't have an id field--is this ok?
    id = models.BigAutoField(
        auto_created=True, primary_key=True, serialize=True)
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)  # need validator
    is_driver = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'user_info'


class DriverInfo(models.Model):
    TYPE_OF_VEHICLE = [
        ('L', 'Large'),
        ('M', 'Medium'),
        ('S', 'Small')
    ]
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    real_name = models.CharField(max_length=64)
    vehicle_type = models.CharField(max_length=1, choices=TYPE_OF_VEHICLE)
    max_capacity = models.IntegerField()
    licence_plate = models.CharField(max_length=32)
    special_info = models.CharField(max_length=128, blank=True, null=True)
    is_del = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'driver_info'
