import datetime
from datetime import datetime
from django.db import transaction
from django.db.models import Q, F
from . import models
MAXIMUM_RIDE = 10
unconfirmed = 0
confirm = 1
completed = 2
cancel_by_user = 3
time_out = 4

ride_status = {
    0: "UNCONFIRMED",
    1: "CONFIRM",
    2: "COMPLETED",
    3: "CANCEL",
    4: "TIME OUT"
}


def build_err(w):
    return {"err": w}


def share_search(dest, etime, ltime, num, user_id):
    ride_data = models.Ride.objects.filter(
        Q(Q(early_arrive_time__lte=etime) & Q(late_arrive_time__gte=etime))
        | Q(Q(early_arrive_time__lte=ltime) & Q(late_arrive_time__gte=ltime)) |
        Q(Q(early_arrive_time__lte=ltime) & Q(early_arrive_time__gte=etime)) |
        Q(Q(late_arrive_time__lte=ltime) & Q(late_arrive_time__gte=etime))
    ).annotate(etime=F('early_arrive_time'),
               ltime=F('late_arrive_time'),
               dest=F('destination'),
               cur_num=F('total_people_num'),
               rest_num=F('max_people_num') - F('total_people_num')
               ).filter(rest_num__gte=num, status=unconfirmed, is_share=1)\
        .exclude(owner_id=user_id).filter(dest__icontains=dest).values("id", "dest", "etime", "ltime", "cur_num", "vehicle_type")
    return ride_data


def driver_search(vehicle_type, max_num, user_id):
    cur_time = datetime.now()
    ride_data = models.Ride.objects.filter(
        vehicle_type__contains=vehicle_type,
        status=unconfirmed,
        total_people_num__lte=max_num,
        late_arrive_time__gt=cur_time
    ).exclude(owner_id=user_id).annotate(etime=F('early_arrive_time'),
                                         ltime=F('late_arrive_time'),
                                         dest=F('destination'),
                                         cur_num=F('total_people_num'),
                                         ).values("id", "dest", "etime", "ltime", "cur_num", "vehicle_type")
    return ride_data


def get_user_by_id(user_id):
    user = models.UserInfo.objects.filter(id=user_id).first()
    return user


def create_ride(user_id, dest, etime, ltime, num, max_num, vehicle_t, detail, is_share):
    ride = models.Ride.objects.all().filter(owner_id=user_id)\
        .filter(Q(status=confirm) | Q(status=unconfirmed))
    if len(ride) > MAXIMUM_RIDE:
        return build_err("out of user maximum active order(10)")
    # new ride
    destination = dest
    early_arrive_time = etime
    late_arrive_time = ltime
    total_people_num = num
    max_people_num = max_num
    create_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    vehicle_type = vehicle_t
    try:
        with transaction.atomic():
            user = get_user_by_id(user_id)
            new_ride_obj = models.Ride.objects.create(
                owner_id=user_id,
                destination=destination,
                early_arrive_time=early_arrive_time,
                late_arrive_time=late_arrive_time,
                status=unconfirmed,
                total_people_num=total_people_num,
                max_people_num=max_people_num,
                detail=detail,
                vehicle_type=vehicle_type,
                is_share=is_share,
                create_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                owner_name=user.name
            )

            order_obj = models.OrderInfo.objects.create(
                user_id=user_id,
                is_owner=1,
                ride_id=new_ride_obj.id,
                status=unconfirmed,
                people_num=total_people_num,
                destination=destination,
                early_arrive_time=early_arrive_time,
                late_arrive_time=late_arrive_time,
                create_time=create_time,
                is_share=is_share,
                user_name=user.name
            )
            return {"ride": new_ride_obj.id, "order": order_obj.id}
    except Exception as e:
        return build_err(e.args)


def check_user_has_join(ride_id, user_id):
    orders = get_all_order_by_ride(ride_id)
    for order in orders:
        if order["user_id"] == user_id:
            return False
    return True


def join_ride(user_id, ride_id, people_num, destination, etime, ltime):
    user = get_user_by_id(user_id)
    ride = get_ride_by_id(ride_id)
    if ride["rest_num"] < people_num:
        return build_err("You can't select the ride, current allow passenger number:" + str(ride["rest_num"]))
    if ride["status"] != unconfirmed:
        return build_err("You can't select the ride, current status is:" + ride_status[ride["status"]])
    if not check_user_has_join(ride_id, user_id):
        return build_err("You have already joined the ride!")
    new_total = ride["cur_num"] + people_num
    try:
        with transaction.atomic():
            models.Ride.objects.filter(id=ride_id).update(
                total_people_num=new_total)
            order_obj = models.OrderInfo.objects.create(
                user_id=user_id,
                is_owner=0,
                ride_id=ride_id,
                status=unconfirmed,
                people_num=people_num,
                destination=destination,
                early_arrive_time=etime,
                late_arrive_time=ltime,
                create_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                is_share=1,
                user_name=user.name
            )
            return {"id": order_obj.id}
    except Exception as e:
        return build_err(e.args)

# values 方法返回 dict


def get_all_order_by_ride(ride_id):
    data = models.OrderInfo.objects.filter(
        ride_id=ride_id
    ).filter(
        Q(status=confirm) | Q(status=unconfirmed) | Q(status=completed)
    ).annotate(etime=F('early_arrive_time'),
               ltime=F('late_arrive_time'),
               dest=F('destination'),
               num=F('people_num'),
               ).values("user_id", "user_name", "dest", "etime", "ltime", "num")
    return data

# return a dict


def get_ride_by_id(ride_id):
    ride = models.Ride.objects.filter(id=ride_id).annotate(
        etime=F('early_arrive_time'),
        onwer_name=F('owner_name'),
        ctime=F('create_time'),
        ltime=F('late_arrive_time'),
        dest=F('destination'),
        cur_num=F('total_people_num'),
        max_num=F('max_people_num'),
        rest_num=F('max_people_num') - F('total_people_num')

    ).values("id", "dest", "etime", "ltime", "ctime", "cur_num", "max_num", "rest_num", "vehicle_type", "status", "owner_name", "detail", "is_share", "driver_name", "driver_id").first()
    return ride


def get_order_by_id(order_id):
    data = models.OrderInfo.objects.filter(id=order_id).first()
    return data


def cancel_order(order_id):
    order = get_order_by_id(order_id)
    try:
        with transaction.atomic():
            ride = get_ride_by_id(order.ride_id)
            if not ride:
                return
            prev_num = ride["cur_num"]
            cur_num = prev_num - order.people_num
            models.OrderInfo.objects.filter(
                id=order_id).update(status=cancel_by_user)
            if order.is_owner:
                models.Ride.objects.filter(
                    id=ride["id"]).update(status=cancel_by_user)
                models.OrderInfo.objects.filter(
                    ride_id=ride["id"]).update(status=cancel_by_user)
            else:
                models.Ride.objects.filter(id=ride["id"]).update(
                    total_people_num=cur_num)
    except Exception as e:
        return build_err(e.args)


def update_order(id, ride_id, dest, num, etime, ltime):
    try:
        order = get_order_by_id(id)
        with transaction.atomic():
            ride = get_ride_by_id(ride_id)
            cur_num = ride["cur_num"] - order.people_num + num
            models.Ride.objects.filter(id=ride_id).update(
                total_people_num=cur_num)
            models.OrderInfo.objects.filter(id=id).update(
                destination=dest,
                people_num=num,
                early_arrive_time=etime,
                late_arrive_time=ltime
            )
    except Exception as e:
        return build_err(e.args)


def update_ride(id, vehicle_type, max_allow_num, is_share, detail):
    data = models.Ride.objects.filter(id=id).update(
        vehicle_type=vehicle_type,
        max_people_num=max_allow_num,
        is_share=is_share,
        detail=detail
    )
    return data


def update_order_by_ride(ride_id, is_share):
    data = models.OrderInfo.objects.filter(
        ride_id=ride_id, is_owner=1).update(is_share=is_share)
    return data


def get_owner_order_by_ride(ride_id):
    data = models.OrderInfo.objects.filter(ride_id=ride_id, is_owner=1).first()
    return data


def get_driver_by_userid(user_id):
    data = models.DriverInfo.objects.filter(user_id=user_id, is_del=0).first()
    return data
# 用这个


def change_ride_status(ride_id, status):
    try:
        with transaction.atomic():
            models.Ride.objects.filter(id=ride_id).update(status=status)
            models.OrderInfo.objects.filter(
                ride_id=ride_id).update(status=status)
    except Exception as e:
        return build_err(e.args)


def confirm_ride(driver_user_id, ride_id):
    driver = get_driver_by_userid(driver_user_id)
    if not driver:
        return build_err("can't find driver info")
    try:
        with transaction.atomic():
            models.Ride.objects.filter(id=ride_id).update(
                status=confirm, driver_id=driver_user_id, driver_name=driver.real_name)
            models.OrderInfo.objects.filter(
                ride_id=ride_id).update(status=confirm)
    except Exception as e:
        return build_err(e.args)


def search_ride_by_special_info(info, max_num, ve_type):
    cur_time = datetime.now()
    data = models.Ride.objects.filter(
        detail__icontains=info,
        status=unconfirmed,
        late_arrive_time__gte=cur_time,
        total_people_num__lte=max_num,
        vehicle_type__contains=ve_type
    ).all()
    return data
