from django.urls import path
from ride_service import views as v
urlpatterns = [
    path('service/', v.ride_test, name="user_search"),
    path('open/', v.ride_open),
    path('waiting', v.ride_wait, name="user_ride_detail"),
    path('order/edit/', v.order_edit),
    path('sharer/edit/', v.share_info_check),
    path('order/history/', v.user_order_history, name="user_history"),
    path('driver/history/', v.driver_ride_history, name="driver_history"),
    path('sharer/search/', v.sharer_ride_search),
    path('driver/search/', v.driver_ride_search, name="driver_search"),
    path('driver/search/detail', v.driver_search_detail),
    path('driver/ride_detail', v.static_ride_detail, name="static_ride_detail"),
    path('driver/confirm/', v.driver_ride_confirm),
    path('driver/complete/', v.driver_ride_complete, name="ride_complete"),
    path('detail', v.ride_detail),
    path('sharer/join/', v.sharer_join_ride),
    path('driver/view_info', v.view_driver_info, name="view_driver_info"),
    path('order/cancel/', v.order_cancel),
    path('edit/', v.ride_edit),
    path('driver/filter', v.driver_filter)


]
