# Generated by Django 4.0.1 on 2022-01-31 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DriverInfo',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('user_id', models.IntegerField()),
                ('real_name', models.CharField(max_length=64)),
                ('vehicle_type', models.CharField(choices=[('L', 'Large'), ('M', 'Medium'), ('S', 'Small')], max_length=1)),
                ('max_capacity', models.IntegerField()),
                ('licence_plate', models.CharField(max_length=32)),
                ('special_info', models.CharField(blank=True, max_length=128, null=True)),
                ('is_del', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'driver_info',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('is_driver', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'user_info',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='OrderInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('is_owner', models.SmallIntegerField()),
                ('ride_id', models.IntegerField()),
                ('status', models.SmallIntegerField()),
                ('people_num', models.IntegerField()),
                ('destination', models.TextField()),
                ('early_arrive_time', models.DateTimeField()),
                ('late_arrive_time', models.DateTimeField()),
                ('create_time', models.DateTimeField()),
                ('is_share', models.SmallIntegerField()),
                ('user_name', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'order_info',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Ride',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner_id', models.IntegerField()),
                ('destination', models.TextField()),
                ('early_arrive_time', models.DateTimeField()),
                ('late_arrive_time', models.DateTimeField()),
                ('status', models.SmallIntegerField()),
                ('total_people_num', models.IntegerField()),
                ('max_people_num', models.IntegerField()),
                ('detail', models.TextField(blank=True, null=True)),
                ('driver_id', models.IntegerField(blank=True, null=True)),
                ('driver_name', models.CharField(blank=True, max_length=20, null=True)),
                ('complete_time', models.DateTimeField(blank=True, null=True)),
                ('create_time', models.DateTimeField()),
                ('vehicle_type', models.CharField(max_length=10)),
                ('is_share', models.SmallIntegerField()),
                ('owner_name', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'ride',
                'managed': True,
            },
        ),
    ]
