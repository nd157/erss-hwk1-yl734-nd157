Danger log:
1.Interaction between users might produce race condition (parallel problem): 
    1.1 two drivers might confirm the same ride almost simultaneously
        Our solution: update the ride's status to check status
        Others: Using an arbitrary algorithm to break the tie
    1.2 two different sharer join same ride / 
        owner cancels ride when joining / 
        User edit ride simultaneously to change passengers number at the same time
        Our solution: use Redis set lock to ride record to when updating rest allowed passenger number and ride status automatically

2.verification of user info: users may register with fake email, and post fake requests which they will not honor; similarly, drivers may register multiple accounts with fake id, and comfirm ride and end up no show. 
Possible solution: limit the number of accounts each ip address may register. Require drivers to upload evidence of their ID and use manual reviewing to check the validity of the driver's license no. Dynamic feedback system that ban users who do not stick to the rules

3. Duplicate Operation:
    3.1 When Driver duplicate confirm or complete a ride
        Our solution: set status in ride to check if the ride has confirmed or completed
    3.2 When Sharer duplicate join a ride
        Our solution: check the list of passengers of that ride, if the sharer is already among them, he/she won't be able to join that ride again.
    3.3 When user duplicate cancel an order
        Our solution: set status in order record to check if the order is cancelled, perform status check before the cancel action, prevent user from cancelling if the status is already cancelled
4.Soft Deletion: users may delete their info by accident
    Our solution: instead of deleting that info directly from the database, we set it to a "is_del=1", which hides it from being searched by the users, but we are able to manually recover it when needed.
 
5.Number of information we can store in server:  All of our data is not solid delete for future history checking. We need to handle the size data more effective.
    Our Solution: 
    1. We store session in user cookies rather than in our server to save space
    2. All of users can only have no more than 10 active ride(confirmed or unconfimred)
    Possible solution: automatically dump outdated(e.g. more than 3 months old) data from our database into disc and delete persistent data.

6.Malicious users may use DDos attack to congest our server, detering other users from accessing it
    Our Solution: Using Nginx to do reverse proxy to permit security
    Possible solution: use reCapcha before every user registration. Increase bandwidth correspondingly.

7.User behavior problem: user join their own ride as sharer or confirm their own ride as driver 
    Our solution: Compare the user id of the owner to the driver each time before confirming the ride. Sharer and driver won't be able to search the ride they initiated.

8. Vicious users view others' infomation by the urls through GET/POST requests, cancel or confirm others' ride and order
    Our solution: All users are required to sign in when using the functions of website
    When requesting a url or doing operations, we will check if the order or ride is belong to the specific user, if not, redirect to /index
    
9.In sync email sending can take up to 30 seconds to send a single email. When dealing with large load in a short amount of time, this may cause a huge delay in response to clients
Our Solution: Using a thread to process sending emails, if it failed, trying sending sync email as backup.
              Using a deamon process based on Celery and assigning task to workers, using Redis as broker to send messages to worker(We implement all two solutions in our code)

10.When performing updates on multiple forms, these update actions may not all succeed. This may cause issues such as driver has confirmed the ride however the order from user side is still marked as open
Our Solution: Using database transaction to make sure whenever one update fails the other successful update will not be performed. 

11.When vicious users request the large size search or get request in a very short time, causing large pressure on our database
Possible solution: Cache templates. 
                   if request always using same parameters or searching existing data, we can use Memcache or Redis to cache data.
                   if request always using different parameters, we can store last request time in Redis with user id / user ip as key to limit request frequency. Or we can use Nginx to set rules to limit the request frequency and ban IP.
                   if request always searching non-existing data, we can use bitmap in Redis to check if the data is available or not to send response.

12.If our database is violating by attackers, causing database leak:
Our solution: we hash the password of our user password, and possibly using des algorithm to hash user's email to aviod data leakage.

13.If the POST request is not sent by our website but others' website:
Our solution: always checking CSRF token when receiving POST request.

14.When moniter the ride status, when it is confimed by a driver or joined by a sharer, all users who is in the ride will get a notification from website. But our browser shouldn't send a time interval to check status in a high frequency, because it will put great pressure on server and database:
Our solution: we use websocket to maintain a long connection between server and client, and if one of client send cancel/confirm/join signal to server, server will notify all ride's users by sending messages. When users close website or go to other pages, we automatically disconnect websocket to save resourses. 

15. Websocket will lose connection if not receiving messages or sending messages
Our solution: we use javascript to automatically send heartbeat package to maintain the connection alive. 

16. The speed to load data from database:
    When the data inside the database is large, the speed of searching data will put great pressure on our database:
Our solution: 
    1. We removed all the foreign key inside our database by writing the logic by ourselves to reduce the pressure on database
    2. We created more index on some key attributes when users and drivers searching records.


    
